package ISP;

import java.math.BigDecimal;

public interface Employee
{

    BigDecimal getBaseAmount();
}
