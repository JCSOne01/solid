/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SRP2;

/**
 *
 * @author Foonkie Monkey
 */
public class DrawableRectangle
{

    private final Rectangle _rectangle;

    public DrawableRectangle(double x, double y, double width, double height)
    {
        _rectangle = new Rectangle(x, y, width, height);
    }

    public Rectangle getRectangle()
    {
        return _rectangle;
    }

    public void Draw()
    {
        System.out.println("Draw the rectangle in the position (" + _rectangle.
                getX() + ", " + _rectangle.getY() + ")");
    }
}
