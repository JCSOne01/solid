package SRP;

public class FuelPump
{

    public void reFuel(Car car, int litersBought)
    {
        while (!car.isFull() && litersBought > 0)
        {
            litersBought--;
            car.increment();
        }
    }

}
