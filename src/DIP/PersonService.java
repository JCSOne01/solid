package DIP;

import java.util.List;

public class PersonService
{

    private PersonRepository personRepository;

    public void setPersonRepository(PersonRepository personRepository)
    {
        this.personRepository = personRepository;
    }

    public List<Person> getAll()
    {
        return personRepository.findAll();
    }

    public boolean add(Person person)
    {
        return personRepository.add(person);
    }

    public Person find(String nickname)
    {
        return personRepository.find(nickname);
    }

}
