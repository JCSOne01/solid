package SRP2;

/**
 *
 * @author Juan Carlos Sierra Rincón
 */
public class Rectangle
{

    private double _x;
    private double _y;
    private double _width;
    private double _height;

    public Rectangle(double x, double y, double width, double height)
    {
        _x = x;
        _y = y;
        _width = width;
        _height = height;
    }

    public double getX()
    {
        return _x;
    }

    public double getY()
    {
        return _y;
    }

    public double getWidth()
    {
        return _width;
    }

    public double getHeight()
    {
        return _height;
    }

    public double getArea()
    {
        return _width * _height;
    }
}
