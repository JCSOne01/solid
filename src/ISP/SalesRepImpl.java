package ISP;

import java.math.BigDecimal;

/**
 *
 * @author Juan Carlos Sierra Rincón
 */
public class SalesRepImpl implements SalesRep
{

    private static final BigDecimal BASE_SALARY = new BigDecimal(80);
    private static final BigDecimal COMMISSION_PERCENTAGE = new BigDecimal(5);
    private final Integer hours;
    private final Integer totalSales;

    public SalesRepImpl(Integer hours, Integer totalSales)
    {
        this.hours = hours;
        this.totalSales = totalSales;
    }

    @Override
    public BigDecimal getBaseAmount()
    {
        return BASE_SALARY.multiply(new BigDecimal(hours));
    }

    @Override
    public BigDecimal getCommission()
    {
        return new BigDecimal(totalSales).multiply(COMMISSION_PERCENTAGE).
                divide(new BigDecimal(100));
    }

}
