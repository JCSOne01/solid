package OCP2;

public abstract class Rectangle
{

    private int _x;
    private int _y;
    private int _width;
    private int _height;

    /**
     *
     * Crear una nueva instancia de rectángulo.
     *
     * @param x      Posición inicial en el eje X.
     * @param y      Posición inicial en el eje Y.
     * @param width  Ancho del rectángulo en pixeles.
     * @param height Alto del rectángulo en pixeles.
     *
     */
    public Rectangle(int x, int y, int width, int height)
    {
        _x = x;
        _y = y;
        _width = width;
        _height = height;
    }

    /**
     *
     * Check if two rectangles overlap.
     *
     * @param rectangle rectangle to see if overlaps with the instance.
     *
     * @return if the two rectangles intersect or not.
     *
     * @autor Denis Petrov Tomado de: https://stackoverflow.com/a/49029463
     *
     */
    public boolean intersects(Rectangle rectangle)
    {
        return rectangle.getX() + rectangle.getWidth() > _x && rectangle.getY()
                + rectangle.getHeight() > _y && _x + _width > rectangle.getX()
                && _y + _height
                > rectangle.getY();
    }

    // Getters & Setters
    /**
     *
     * Obtener la posición en X del rectángulo.
     *
     * @return la posición del rectángulo en el canvas con respecto al eje X.
     *
     */
    public int getX()
    {
        return _x;
    }

    /**
     *
     * Cambiar la posición en X del rectángulo.
     *
     * @param x nueva posición en X.
     *
     */
    public void setX(int x)
    {
        if (x < 0 || x > 354)
        {
            return;
        }
        _x = x;
    }

    /**
     *
     * Obtener la posición en Y del rectángulo.
     *
     * @return la posición del rectángulo en el canvas con respecto al eje Y.
     *
     */
    public int getY()
    {
        return _y;
    }

    /**
     *
     * Cambiar la posición en Y del rectángulo.
     *
     * @param y nueva posición en Y.
     *
     */
    public void setY(int y)
    {
        _y = y;
    }

    /**
     *
     * Obtener el ancho del rectángulo.
     *
     * @return Ancho del rectángulo en pixeles.
     *
     */
    public int getWidth()
    {
        return _width;
    }

    /**
     *
     * Obtener el alto del rectángulo.
     *
     * @return Alto del rectángulo en pixeles.
     *
     */
    public int getHeight()
    {
        return _height;
    }
}
