package ISP;

import java.math.BigDecimal;

public interface Partner extends Employee
{

    BigDecimal getProfits();
}
