package SRP;

public class Car
{

    private static final int MAX_FUEL = 40;
    private int fuel;

    public void increment()
    {
        this.fuel++;
    }

    public int getFuel()
    {
        return fuel;
    }

    public void spendFuel(int fuelSpent)
    {
        if (fuelSpent > fuel)
        {
            fuel = 0;
            return;
        }
        fuel -= fuelSpent;
    }

    public boolean isFull()
    {
        return fuel == MAX_FUEL;
    }

    public boolean isEmpty()
    {
        return fuel == 0;
    }

}
