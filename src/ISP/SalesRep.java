package ISP;

import java.math.BigDecimal;

/**
 *
 * @author juan Carlos Sierra Rincón
 */
public interface SalesRep extends Employee
{
    BigDecimal getCommission();
}
