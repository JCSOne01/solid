package OCP2;

import com.sun.glass.events.KeyEvent;
import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

public class Player extends Ship
{

    private final ArrayList<Bullet> _bullets;

    /**
     *
     * Crear una nueva instancia de jugador.
     *
     * @param x         Posición inicial en el eje X.
     * @param y         Posición inicial en el eje Y.
     * @param width     Ancho de la nave en pixeles.
     * @param height    Alto de la nave en pixeles.
     * @param color     Color con el que se dibujará la nave del jugador.
     *
     */
    public Player(int x, int y, int width, int height, Color color)
    {
        super(x, y, width, height, color);
        _bullets = new ArrayList<>();
    }

    /**
     *
     * Dibuja la nave del jugador y sus componentes.
     *
     * @param g Elemento "Graphics" del canvas sobre el cual se dibujará.
     *
     */
    @Override
    public void draw(Graphics g)
    {
        super.draw(g);

        // Dibujar las balas que ha disparado el jugador.
        _bullets.forEach((bullet) -> 
        {
            bullet.draw(g);
        });
    }

    /**
     *
     * Actualizar la posición de la nave del jugador y sus componentes.
     *
     */
    @Override
    public void update()
    {
        // Recorrer la lista de balas.
        _bullets.forEach((bullet) -> 
        {
            // actualizar la posición de la bala.
            bullet.update();
        });

        // Remover del listado todas las balas que están fuera del canvas o que
        // han colisionado con un enemigo.
        _bullets.removeIf((bullet) -> !bullet.isAlive());
    }

    /**
     *
     * Actualizar el estado de la nave del jugador y sus componentes.
     *
     * @param input componente de teclado.
     *
     */
    public void update(KeyboardInput input)
    {

        // Invocar el método update.
        update();

        // Actualizar el estado del teclado.
        input.poll();

        // Comprobar el estado del teclado.
        if (input.keyDown(KeyEvent.VK_A))
        {
            // Se presiono la tecla "A", mover la nave del jugador a la izquierda.
            setX(getX() - 3);
        }
        else if (input.keyDown(KeyEvent.VK_D))
        {
            // Se presiono la tecla "D", mover la nave del jugador a la derecha.
            setX(getX() + 3);
        }
        if (input.keyDownOnce(KeyEvent.VK_SPACE))
        {
            // Se presiono la tecla "Espacio", disparar una nueva bala.
            _bullets.add(new Bullet(getX() + 21, getY() + 7));
        }
    }
}
