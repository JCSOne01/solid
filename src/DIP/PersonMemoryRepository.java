package DIP;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PersonMemoryRepository implements PersonRepository
{

    private final List<Person> persons;

    public PersonMemoryRepository()
    {
        persons = new ArrayList<>();
    }

    @Override
    public boolean add(Person person)
    {
        if (find(person.getNickname()) != null)
        {
            return false;
        }

        persons.add(person);
        return true;
    }

    @Override
    public Person find(String nickname)
    {
        Optional<Person> person = persons.stream().filter(p -> p.getNickname().
                equals(nickname)).findFirst();
        return person.isPresent() ? person.get() : null;
    }

    @Override
    public List<Person> findAll()
    {
        return persons;
    }

}
