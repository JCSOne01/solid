package OCP2;

import java.awt.Color;
import java.awt.Graphics;

public class Bullet extends Rectangle
{

    private boolean _isAlive;
    private Thread _soundThread;

    /**
     *
     * Crear una nueva instancia de bala.
     *
     * @param x Posición inicial en X.
     * @param y Posición inicial en Y.
     *
     */
    public Bullet(int x, int y)
    {
        super(x, y, 2, 4);
        _isAlive = true;
        playSound();
    }

    /**
     *
     * Play bullet sound when it is shot.
     *
     */
    private void playSound()
    {
        // Play sound
    }

    /**
     *
     * Dibujar la bala en el canvas.
     *
     * @param g Elemento "Graphics" del canvas sobre el cual se dibujará.
     *
     */
    public void draw(Graphics g)
    {
        g.setColor(Color.WHITE);
        g.fillRect(getX(), getY(), getWidth(), getHeight());
    }

    /**
     *
     * Actualizar la posición de la bala.
     *
     */
    public void update()
    {
        setY(getY() - 3);

        // Comprobar si la bala no se ha salido del canvas, si lo ha hecho,
        // cambiar la propiedad _isAlive para indicar que ya no debe existir en
        // el juego.
        if (getY() == 0)
        {
            _isAlive = false;
        }
    }

    /**
     *
     * Comprobar si la bala sigue en juego
     *
     * @return si la bala está activa.
     *
     */
    public boolean isAlive()
    {
        return _isAlive;
    }
}
