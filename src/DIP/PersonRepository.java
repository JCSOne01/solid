package DIP;

import java.util.List;

public interface PersonRepository
{

    boolean add(Person person);

    Person find(String nickname);

    List<Person> findAll();
}
