/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SolidDesignPrinciples;

import DIP.Person;
import DIP.PersonMemoryRepository;
import DIP.PersonService;
import ISP.Contractor;
import ISP.ContractorImpl;
import ISP.Partner;
import ISP.PartnerImpl;
import ISP.SalesRep;
import ISP.SalesRepImpl;
import SRP.Car;
import SRP.FuelPump;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Juan Carlos Sierra Rincón
 */
public class SolidDesignPrinciples
{

    private final Scanner scan;

    public SolidDesignPrinciples()
    {
        scan = new Scanner(System.in);
    }

    public void testDip()
    {
        int option = 0;
        PersonService pService = new PersonService();
        pService.setPersonRepository(new PersonMemoryRepository());
        while (option != 4)
        {
            System.out.println();
            System.out.println(
                    "Por favor seleccione la tarea que quiere realizar");
            System.out.println("1. Agregar persona.");
            System.out.println("2. Buscar persona.");
            System.out.println("3. Listar todas las personas.");
            System.out.println("4. Salir.");
            option = readInt("Ingrese su opción");
            switch (option)
            {
                case 1:
                {
                    String nickname = "";
                    while ("".equals(nickname))
                    {
                        System.out.println("Ingrese el nickname de la persona");
                        nickname = scan.next();
                    }
                    if (pService.add(new Person(nickname)))
                    {
                        System.out.println("La persona ha sido agregada");
                    }
                    else
                    {
                        System.out.println(
                                "La persona que está intentando agregar ya existe");
                    }
                    break;
                }
                case 2:
                {
                    String nickname = "";
                    while (!"".equals(nickname))
                    {
                        System.out.println(
                                "Ingrese el nickname de la persona que quiere buscar");
                        nickname = scan.nextLine();
                    }
                    if (pService.find(nickname) != null)
                    {
                        System.out.println("La persona está en la lista");
                    }
                    else
                    {
                        System.out.println("No se ha encontrado a la persona");
                    }
                    break;
                }
                case 3:
                {
                    List<Person> people = pService.getAll();
                    System.out.println("Personas:");
                    people.forEach((person) -> 
                    {
                        System.out.println(person.getNickname());
                    });
                    break;
                }
            }
        }
    }

    public void testIsp()
    {
        int option = 0;
        while (option != 7)
        {
            List<Partner> partners = new ArrayList<>();
            List<Contractor> contractors = new ArrayList<>();
            List<SalesRep> salesReps = new ArrayList<>();
            System.out.println();
            System.out.println(
                    "Por favor seleccione la tarea que quiere realizar");
            System.out.println("1. Agregar socio.");
            System.out.println("2. Agregar contratista.");
            System.out.println("3. Agregar representante de ventas.");
            System.out.println("4. Obtener pago total a socios.");
            System.out.println("5. Obtener pago total a contratistas.");
            System.out.println(
                    "6. Obtener pago total a representantes de ventas.");
            System.out.println("7. Salir.");
            option = readInt("Ingrese su opción");
            switch (option)
            {
                case 1:
                {
                    int hours = readInt("Ingrese el número de horas trabajadas");
                    partners.add(new PartnerImpl(hours));
                    break;
                }
                case 2:
                {
                    int hours = readInt("Ingrese el número de horas trabajadas");
                    contractors.add(new ContractorImpl(hours));
                    break;
                }
                case 3:
                {
                    int hours = readInt("Ingrese el número de horas trabajadas");
                    int totalSales = readInt("Ingrese el monto total de ventas");
                    salesReps.add(new SalesRepImpl(hours, totalSales));
                    break;
                }
                case 4:
                {
                    BigDecimal totalPartnersSalary = partners.stream().map(p
                            -> p.getBaseAmount().add(p.getProfits()))
                            .reduce(BigDecimal.ZERO, BigDecimal::add);
                    System.out.println("El pago total para los socios es de: $"
                            + totalPartnersSalary);
                }
                case 5:
                {
                    BigDecimal totalContractorsSalary = contractors.stream().
                            map(p -> p.getBaseAmount().add(p.getBonus())).
                            reduce(BigDecimal.ZERO, BigDecimal::add);
                    System.out.println(
                            "El pago total para los contratistas es de: $"
                            + totalContractorsSalary);
                }
                case 6:
                {
                    BigDecimal totalSalesRepsSalary = salesReps.stream().
                            map(p -> p.getBaseAmount()).
                            reduce(BigDecimal.ZERO, BigDecimal::add);
                    BigDecimal totalSalesRepsCommissions = salesReps.stream().
                            map(p -> p.getCommission()).
                            reduce(BigDecimal.ZERO, BigDecimal::add);
                    System.out.println(
                            "El pago total para los contratistas es de: $"
                            + totalSalesRepsSalary + ", con $"
                            + totalSalesRepsCommissions + " de comisiones");
                }
            }
        }
    }

    public void testLsp()
    {
        int option = 0;
        while (option != 4)
        {
            System.out.println();
            System.out.println(
                    "Por favor seleccione la tarea que quiere realizar");
            System.out.println("1. Obtener salario de los ingenieros.");
            System.out.println("2. Obtener salario de los vendedores.");
            System.out.println("3. Obtener salario de los gerentes.");
            System.out.println("4. Salir.");
            option = readInt("Ingrese su opción");
            switch (option)
            {
                case 1:
                {
                    System.out.println("El salario de un ingeniero es de $"
                            + new LSP.Engineer().getPaymentAmount());
                    break;
                }
                case 2:
                {
                    System.out.println("El salario de un vendedor es de $"
                            + new LSP.Salesman().getPaymentAmount());
                    break;
                }
                case 3:
                {
                    System.out.println("El salario de un gerente es de $"
                            + new LSP.Manager().getPaymentAmount());
                    break;
                }
            }
        }
    }

    public void testOcp()
    {
        int option = 0;
        while (option != 4)
        {
            System.out.println();
            System.out.println(
                    "Por favor seleccione la tarea que quiere realizar");
            System.out.println("1. Obtener salario de los ingenieros.");
            System.out.println("2. Obtener salario de los vendedores.");
            System.out.println("3. Obtener salario de los gerentes.");
            System.out.println("4. Salir.");
            option = readInt("Ingrese su opción");
            switch (option)
            {
                case 1:
                {
                    System.out.println("El salario de un ingeniero es de $"
                            + new OCP.Engineer().getPaymentAmount());
                    break;
                }
                case 2:
                {
                    OCP.Salesman salesman = new OCP.Salesman();
                    System.out.println("El salario de un vendedor es de $"
                            + salesman.getPaymentAmount() + " del cual, $"
                            + salesman.getCommission()
                            + " corresponden a su comisión");
                    break;
                }
                case 3:
                {
                    OCP.Manager manager = new OCP.Manager();
                    System.out.println("El salario de un gerente es de $"
                            + manager.getPaymentAmount() + " del cual, $"
                            + manager.getBonus()
                            + " corresponden a su bonificación");
                    break;
                }
            }
        }
    }

    public void testSrp()
    {
        int option = 0;
        Car car = new Car();
        while (option != 6)
        {
            System.out.println();
            System.out.println(
                    "Por favor seleccione la tarea que quiere realizar");
            System.out.println("1. Tanquear carro.");
            System.out.println("2. Obtener medición de combustible.");
            System.out.println("3. Usar combustible.");
            System.out.println("4. Comprobar si el tanque está vacio.");
            System.out.println("5. Comprobar si el tanque está lleno.");
            System.out.println("6. Salir.");
            option = readInt("Ingrese su opción");
            switch (option)
            {
                case 1:
                {
                    int litersBought = readInt(
                            "Ingrese la cantidad de litros que desea tanquear");
                    FuelPump pump = new FuelPump();
                    pump.reFuel(car, litersBought);
                    break;
                }
                case 2:
                {
                    System.out.println("El carro tiene " + car.getFuel()
                            + " litros de combustible");
                    break;
                }
                case 3:
                {
                    int litersSpent = readInt(
                            "Ingrese la cantidad de litros que se usaron");
                    car.spendFuel(litersSpent);
                    break;
                }
                case 4:
                {
                    System.out.println("El tanque"
                            + (car.isEmpty() ? "" : " no") + " está vacio");
                    break;
                }
                case 5:
                {
                    System.out.println("El tanque" + (car.isFull() ? "" : " no")
                            + " está lleno");
                    break;
                }
            }
        }
    }

    private int readInt(String message)
    {
        int i = -1;
        while (i == -1)
        {
            try
            {
                System.out.println(message);
                i = scan.nextInt();
            }
            catch (Exception e)
            {
                System.out.println("Selección no válida");
            }
        }
        return i;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        int option = 0;
        SolidDesignPrinciples sdp = new SolidDesignPrinciples();
        while (option != 6)
        {
            System.out.println(
                    "Por favor seleccione el principio SOLID que quiere probar");
            System.out.println("1. Inversión de dependencias.");
            System.out.println("2. Segregación de interfaces.");
            System.out.println("3. Sustitución de Liskov.");
            System.out.println("4. Abierto/Cerrado");
            System.out.println("5. Responsabilidad única.");
            System.out.println("6. Salir.");
            try
            {
                option = scan.nextInt();
                System.out.println();
            }
            catch (Exception e)
            {
                System.out.println("Selección no válida");
            }
            switch (option)
            {
                case 1:
                {
                    sdp.testDip();
                    break;
                }
                case 2:
                {
                    sdp.testIsp();
                    break;
                }
                case 3:
                {
                    sdp.testLsp();
                    break;
                }
                case 4:
                {
                    sdp.testOcp();
                    break;
                }
                case 5:
                {
                    sdp.testSrp();
                    break;
                }
            }
        }
        System.out.println("Hasta pronto!");
    }

}
